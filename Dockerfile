FROM php:7.4-apache

RUN apt-get update -yqq && apt-get -y install libxml2-dev

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        curl \
        zip \
        unzip \
        gnupg

RUN docker-php-ext-install pdo_mysql sockets
RUN apt-get update

# Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# NodeJS
RUN curl -sL https://deb.nodesource.com/setup_14.x  | bash -
RUN apt-get -y install nodejs
RUN npm install

# Yarn
RUN npm install -g yarn

#RUN npm install vue-loader@^15.0.11 vue-template-compiler --save-dev
#RUN npm install @symfony/webpack-encore vue vue-router vuetify
#RUN npm install axios

