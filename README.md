# README HAYA PROJECT #

En este documento se indica las técnologias usadas, el entorno y el porque de ello.

### Entorno Docker ###

El proyecto se encuentra ejecutable desde el docker que incorpora.

Se deberán seguir los siguientes pasos para levantar docker:

* No tener otros contenedores levantados.
* Ejecutar en consola: *docker-compose build*
* Posteriormente ejecutar: *docker-compose up* con añadiendo al final -d
* Entrar en el contenedor con *docker exec -it haya_app bash*
* Ejecutar * composer install *  
* Se añaden tres comandos más (están abajo). 
* Una vez dentro ejecutar *yarn build*
* Creamos DB *bin/console doctrine:database:create*
* Creamos schema *bin/console doctrine:schema:update --force*
* Ejecutar el fixture para rellenar la base de datos *doctrine:fixtures:load* (yes)

comandos a ejecutar en consola (ya indicados anteriormente):
```
docker-compose build
docker-compose up
docker exec -it haya_app bash
composer install
npm install vue-loader@^15.0.11 vue-template-compiler --save-dev
npm install @symfony/webpack-encore vue vue-router vuetify
npm install axios
yarn build
bin/console doctrine:database:create
bin/console doctrine:schema:update --force
bin/console doctrine:fixtures:load
```
  
Ahora tiene el proyecto levantado que podrá verlo en [Ver proyecto](http://localhost:8000) o **http://localhost:8000**

### Introducción a las tecnologías, arquitectura y patrones de diseño utilizados ###

Sinceramente para un ejercicio no muy extenso, no he pensado en ninguna arquitectura o patrón de diseño.
Se ha usado Symfony como core, donde se usó las siguientes tecnologías:

* El ORM de Doctrine (MySQL) para la creación de las entidades y sus relaciones
* Api_platform para la gestión de las API. Realmente iba a hacerlo con FosRestbundle o a mano inyectando las rutas con fosjsrouting, pero recordé que comentasteis algo de Api_platform y como nunca lo había tocado, vi que era buen momento.
* Vue para la visualización de los datos.

### Problemas con los que te has encontrado ###

Realmente el mayor problema era que no sabía como funcionaba Api_platform y plantear bien Vue para evitar los eventos para comunicar con los padres (por comentar algo).

### Escribe las anotaciones que creas necesarias ###

Si fuese un proyecto para llevar acabo habría que mirar tema de versionado de las API.
Leí que varnish no es muy difícil de configurar (ves no metí cache :D).
Solo puse un filtro de prueba (por no alargar mucho el tiempo), pero todos los datos que van en los filtros se pueden meter en redis.

Bueno en conclusión se puede hacer de mil maneras.
Nada más y la verdad que me gustaría tener vuestra opinión sobre el proyecto/ejercicio para seguir aprendiendo. Y seguiré dando caña a Api_platform para ver cositas jajajaja.
