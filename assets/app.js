import Vue from 'vue';
import Home from './Vue/home.vue';

new Vue({
    el: '#main',
    components: { Home },
    template: '<Home></Home>'
    // render: h => h(Home)
});
