<?php

namespace App\DataFixtures;

use App\Entity\Advertiser;
use App\Entity\Conservation;
use App\Entity\Home;
use App\Entity\Province;
use App\Entity\Rating;
use App\Entity\Target;
use App\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppFixtures extends Fixture
{
    /** @var ObjectManager */
    private $om;

    private $output;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

    }

    public function load(ObjectManager $manager)
    {
        $this->om = $manager;
        $this->output = new ConsoleOutput();

        $this->loadProvinces();
        $this->loadTarget();
        $this->loadAdvertiser();
        $this->loadType();
        $this->loadRating();
        $this->loadConservation();
        $this->loadHome();

    }

    private function loadHome()
    {
        $this->output->writeln('Cargando las viviendas');

        $titles = [
            'Oportunidad única, chalet independiente',
            'me lo quitan de las manos',
            'vivienda digna'
        ];

        $description = [
            'Maecenas porta sem lectus, vitae posuere tortor tempor at. Phasellus auctor bibendum mi, in condimentum leo maximus ut. Duis sit amet mollis velit. Vivamus ut facilisis ipsum. Pellentesque facilisis finibus arcu ac dapibus. Quisque urna lorem, venenatis id eros quis, mollis dapibus est. Quisque lacinia consequat sapien eu scelerisque. Aenean a nisl elementum, tempus dui ac, tincidunt urna. Nulla quis tristique elit.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rhoncus, augue sed tristique iaculis, lorem tortor mollis risus, ac feugiat augue ex pellentesque erat. Maecenas id blandit risus. Etiam eget tortor facilisis, porttitor ex vel, sollicitudin felis. Donec aliquam accumsan ligula et accumsan.',
            'Etiam consectetur massa nec justo varius ultrices. Nulla in ultrices tellus, non iaculis urna. Duis non libero euismod, pretium lorem sit amet, gravida dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam a lacus aliquet, porttitor ante at, tincidunt enim. Sed a risus nec libero pretium pulvinar ut sed nisi. Nam et convallis ex. Nunc sed dolor gravida, convallis quam vel, elementum leo.'
        ];

        $address = [
            'calle leopoldo',
            'Avenida Europa 5',
            'Calle hasta aquí'
        ];

        foreach ($titles as $title) {
            $home = new Home();
            $home->setTitle($title);
            $randDescr = array_rand($description);
            $home->setDescription($description[$randDescr]);
            $randAddress = array_rand($address);
            $home->setAddress($address[$randAddress]);
            $home->setPrice(random_int(100000, 999999));
            $home->setRoom(random_int(1, 5));
            $home->setBath(random_int(1, 3));
            $home->setYear(random_int(1980, 2021));
            $home->setSize(random_int(70, 150));
            $home->setProvince($this->om->getRepository(Province::class)->find(random_int(1,50)));
            $home->setRating($this->om->getRepository(Rating::class)->find(random_int(1,6)));

            unset($description[$randDescr]);
            unset($address[$randAddress]);
            $this->om->persist($home);
        }
        $this->om->flush();

        $this->output->writeln('Finalizado las viviendas');
    }

    private function loadConservation()
    {
        $this->output->writeln('Cargando tipos de conservaciones');
        $items = ['Nuevo', 'seminuevo', 'a reformar'];
        $this->createEntityOnlyName($items, Conservation::class);

        $this->output->writeln('Finalizado tipos de conservaciones');
    }

    private function loadRating()
    {
        $this->output->writeln('Cargando calificaciones energéticas');

        $this->resetId('rating');

        $items = [
            'A' => 'green',
            'B' => 'green',
            'C' => 'green',
            'D' => 'yellow',
            'E' => 'orange',
            'F' => 'red',
        ];

        foreach ($items as $lyrics => $color) {
            $rating = new Rating();
            $rating->setLyrics($lyrics);
            $rating->setColor($color);

            $this->om->persist($rating);
            $this->om->flush();
        }
        $this->output->writeln('Finalizado calificaciones energéticas');
    }

    private function loadType()
    {
        $this->output->writeln('Cargando tipos de inmuebles');
        $items = ['Piso', 'Chalet'];
        $this->createEntityOnlyName($items, Type::class);

        $this->output->writeln('Finalizado tipos de inmuebles');
    }

    private function loadProvinces()
    {
        $this->output->writeln('Cargando provincias');

        $this->resetId('province');

        $items = ['Álava','Albacete','Alicante','Almería','Ávila','Badajoz','Islas Baleares','Barcelona','Burgos','Cáceres','Cádiz','Castellón','Ciudad Real','Córdoba','A Coruña','Cuenca','Gerona','Granada','Guadalajara','Gipuzkoa','Huelva','Huesca','Jaén','León','Lérida','La Rioja','Lugo','Madrid','Málaga','Murcia','Navarra','Ourense','Asturias','Palencia','Las Palmas','Pontevedra','Salamanca','Santa Cruz de Tenerife','Cantabria','Segovia','Sevilla','Soria','Tarragona','Teruel','Toledo','Valencia','Valladolid','Vizcaya','Zamora','Zaragoza','Ceuta','Melilla'];
        $this->createEntityOnlyName($items, Province::class);

        $this->output->writeln('Finalizado provincias');
    }

    private function loadTarget()
    {
        $this->output->writeln('Cargando targets');
        $items = ['Alquilar', 'Comprar'];
        $this->createEntityOnlyName($items, Target::class);

        $this->output->writeln('Finalizado targets');
    }

    private function loadAdvertiser()
    {
        $this->output->writeln('Cargando advertiser');
        $advertiser[] = [
            'name'=>'Bankia',
            'phone'=>'+34916589878',
            'email'=>'inmuebles@bankia.es'
        ];
        $advertiser[] = [
            'name'=>'Santander',
            'phone'=>'916585874',
            'email'=>'inmuebles@santander.es'
        ];
        $advertiser[] = [
            'name'=>'Unicaja',
            'phone'=>'658987845',
            'email'=>'inmuebles@unicaja.es'
        ];

        foreach ($advertiser as $item) {
            $advertiser = new Advertiser();
            $advertiser->setName($item['name']);
            $advertiser->setEmail($item['email']);
            $advertiser->setPhone($item['phone']);

            $this->om->persist($advertiser);
        }
        $this->om->flush();

        $this->output->writeln('Finalizado advertiser');
    }

    private function createEntityOnlyName($items, $entity)
    {
        foreach ($items as $item) {
            $province = new $entity();
            $province->setName($item);

            $this->om->persist($province);
        }
        $this->om->flush();
    }

    private function resetId($table)
    {
        $connection = $this->em->getConnection();
        $connection->executeQuery("ALTER TABLE $table AUTO_INCREMENT = 1");
    }

}
