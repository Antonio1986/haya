<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\HomeRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=HomeRepository::class)
 * @ApiResource(
 *      collectionOperations={"get"={"normalization_context"={"groups"="home:list"}}},
 *      itemOperations={"get"={"normalization_context"={"groups"="home:item"}}},
 *      attributes={"filters"={"home.search_filter"}},
 *      paginationEnabled=true
 * )
 * @ApiFilter(SearchFilter::Class, properties={"room":"exact"})
 */
class Home
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"home:list", "home:item"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"home:list", "home:item"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"title","year"})
     * @Groups({"home:list", "home:item"})
     */
    private $slug;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"home:list", "home:item"})
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"home:list", "home:item"})
     */
    private $room;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"home:list", "home:item"})
     */
    private $bath;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"home:list", "home:item"})
     */
    private $year;

    /**
     * @ORM\Column(type="text")
     * @Groups({"home:item"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Province::class, inversedBy="homes")
     * @Groups({"home:item"})
     */
    private $province;

    /**
     * @ORM\Column(type="float")
     * @Groups({"home:list", "home:item"})
     */
    private $size;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"home:item"})
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity=Rating::class)
     * @Groups({"home:item"})
     */
    private $rating;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRoom(): ?int
    {
        return $this->room;
    }

    public function setRoom(int $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getBath(): ?int
    {
        return $this->bath;
    }

    public function setBath(int $bath): self
    {
        $this->bath = $bath;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProvince(): ?Province
    {
        return $this->province;
    }

    public function setProvince(?Province $province): self
    {
        $this->province = $province;

        return $this;
    }

    public function getSize(): ?float
    {
        return $this->size;
    }

    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getRating(): ?Rating
    {
        return $this->rating;
    }

    public function setRating(?Rating $rating): self
    {
        $this->rating = $rating;

        return $this;
    }
}
