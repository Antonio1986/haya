<?php

namespace App\Repository;

use App\Entity\Conservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Conservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conservation[]    findAll()
 * @method Conservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conservation::class);
    }

    // /**
    //  * @return Conservation[] Returns an array of Conservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Conservation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
